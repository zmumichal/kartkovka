package controllers

import models.{Answer, Question, Quiz, QuizInstance}
import persistence.dao.{QuizDAO, QuizInstanceDAO}
import play.api.data.Form
import play.api.data.Forms._
import play.api.libs.concurrent.Execution.Implicits._
import play.api.mvc._
import services.impl.{QuizInstantiationServiceImpl, QuizProgressServiceImpl}

object Solving extends Controller {
  def index(id: String) = Action.async {
    QuizInstanceDAO.findById(id.toLong).flatMap((instance: QuizInstance) =>
      QuizProgressServiceImpl.getQuestions(instance.id.get).map((map: Map[Question, Seq[Answer]]) =>
        Ok(views.html.solving(instance, map))
      )
    )
  }

  val answersForm = Form {
    tuple(
      "instance-id" -> text,
      "answer-ids" -> seq(number),
      "answer-values" -> seq(boolean)
    )
  }
  val logInForm = Form {
    tuple(
      "user-id" -> text,
      "token" -> text
    )
  }

  def submit(id: String) = Action.async { implicit request =>

    val (instanceId, answerIds, choices) = answersForm.bindFromRequest.get
    val answerIdToChoice: Map[Long, Boolean] = (answerIds.map(x => x.toLong), choices).zipped.toMap
    val sessionId: Option[String] = request.session.get("session-id")

    QuizProgressServiceImpl.updateResults(instanceId.toLong, answerIdToChoice, sessionId.get, request.remoteAddress).map {
      case true => Redirect(routes.Solving.index(id))
      case false => Unauthorized
    }
  }

  def login() = Action { implicit request =>
    Ok(views.html.login())
  }

  def loginSubmit() = Action.async { implicit request =>
    val (userId, token) = logInForm.bindFromRequest.get
    val uuid = java.util.UUID.randomUUID().toString()

    QuizDAO.findByToken(token).flatMap {
      case quiz: Quiz => QuizInstantiationServiceImpl.instantiate(quiz.id.get, userId, request.remoteAddress, uuid).map {
        case null => Ok(views.html.login())
        case instance => Redirect(routes.Solving.index(instance.id.get.toString)) withSession (
          request.session +("session-id", uuid)
          )
      }
    }
  }
}