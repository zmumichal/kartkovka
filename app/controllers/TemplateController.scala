package controllers

import controllers.Dummies._
import persistence.dao.{AnswerTemplateDAO, QuestionTemplateDAO, QuizTemplateDAO}
import play.api.data.Form
import play.api.data.Forms._
import play.api.mvc.{Action, Controller}
import services.impl.QuizCreationServiceImpl

import scala.concurrent.ExecutionContext.Implicits.global

import scala.concurrent.Future

object TemplateController extends Controller {
  // można zostawić, zeby było wiadomo jak wygląda
  val sample_quiz = QuizForm("Mój przykładowy szablon", List("Pytanie nr 1\n+dobra odpowiedź\n-zła odpowiedź", "Pytanie nr 2\n+dobra odpowiedź\n-zła odpowiedź"))
  val quiz_placeholder = QuizForm("Nazwa szablonu", List())

  def showEmpty = Action {
    Ok(views.html.templateEdit(quiz_placeholder))
  }

  def showExisting(templateId: String) = Action.async {
    val quizTemplate = QuizTemplateDAO.findById(templateId.toLong)

    val something = quizTemplate map {
      template =>
        val questions = QuestionTemplateDAO.findByQuizTemplateId(template.id.get)
        questions map {
          realQuestions =>
            val realQuestionsMapped = realQuestions map {
              question =>
                val answers = AnswerTemplateDAO.findByQuestionTemplateId(question.id.get)
                answers map {
                  realAnswers =>
                    val answersText = realAnswers.map {
                      answer =>
                        val prefix: String = if (answer.correct) "+" else "-"
                        prefix + answer.answer
                    } mkString "\n"
                    question.question + "\n" + answersText
                }
            }
            Future(template.name) +: realQuestionsMapped
        }
    }

    val flattedSomething = something.flatMap(identity).map(Future.sequence(_)).flatMap(identity)
    flattedSomething map { element => Ok(views.html.templateEdit(QuizForm(element.head, element.tail.toList))) }
  }

  val answerForm = Form(
    mapping(
      "name" -> text,
      "content" -> text
    )((name: String, content: String) => {
      QuizForm(name, content.split(",").toList)
    })((quiz: QuizForm) => {
      Option((quiz.name, quiz.questions.mkString("")))
    })
  )

  def modify(id: String) = Action.async { implicit request =>
    val quizForm = answerForm.bindFromRequest.get
    val questions = quizForm.questions

    QuizCreationServiceImpl.updateQuizTemplate(id.toLong, questions) map { id =>
      Redirect("/edit")
    }
  }

  def create(id: String = "0") = Action.async { implicit request =>
    val quizForm = answerForm.bindFromRequest.get
    val questions = quizForm.questions
    println("Nazwa: " + quizForm.name)
    println("Pytania:")
    questions.foreach({ question => println(question) })

    val templateId = QuizCreationServiceImpl.createQuiz(quizForm.name)
    println("New template id " + templateId)
    //    questions.foreach(question => QuizCreationServiceImpl.addQuestionWithAnswers(templateId, question))
    QuizCreationServiceImpl.addQuestionsWithAnswers(templateId, questions) map { i => Redirect("/edit") }
  }
}
