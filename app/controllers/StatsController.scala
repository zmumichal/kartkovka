package controllers

import persistence.dao.QuizInstanceDAO
import play.api.mvc.{Action, Controller}
import scala.concurrent.ExecutionContext.Implicits.global

object StatsController extends Controller {
  def index = Action.async {
    QuizInstanceDAO.getNLatest(10) map { list =>
      val groupedResults = list.groupBy(_.quizId)
      Ok(views.html.completed(groupedResults))
    }
  }
}
