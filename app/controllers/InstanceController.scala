package controllers

import play.api.mvc.{Action, Controller}
import services.impl.{QuizInstantiationServiceImpl, QuizCreationServiceImpl}
import scala.concurrent.ExecutionContext.Implicits.global

object InstanceController extends Controller {
  def index = Action.async {
    val futureList = QuizCreationServiceImpl.listQuizzes()
    futureList.map({list => Ok(views.html.templateSelect(list))})
  }

  def submit = Action { implicit request =>
    val selectedValue = request.body.asFormUrlEncoded.get("select").head
    println(selectedValue)
    Redirect("/begin/" + selectedValue)
  }

  def showDetails(name: String) = Action { implicit request =>
    Ok(views.html.instanceConfiguration(name));
  }

  def submitDetails(name: String) = Action {
    implicit request =>
      val durationSeconds = request.body.asFormUrlEncoded.get("time").head.toInt
      val questions = request.body.asFormUrlEncoded.get("questions").head.toInt
      val answers = request.body.asFormUrlEncoded.get("answers").head.toInt
      val ipMask = request.body.asFormUrlEncoded.get("mask").head

      println("User selected" + name + " for time of " + durationSeconds + " with questions: "
        + questions +" with answers: " + answers)

      val token = QuizInstantiationServiceImpl.createQuiz(name.toLong, durationSeconds, questions, answers, ipMask)

      Ok(views.html.instanceStarted(name, durationSeconds, questions, answers, token))
  }
}
