package controllers

import play.api.mvc.{Action, Controller}
import services.impl.QuizCreationServiceImpl
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.util.Success

object CRUDController extends Controller {
  def listTemplates = Action.async {
    val futureList = QuizCreationServiceImpl.listQuizzes()
    futureList.map({list => Ok(views.html.templateEditList(list))})
  }

  def editOrRemoveTemplate = Action.async { request =>
    if (request.body.asFormUrlEncoded.get.contains("edit")) {
      val id = request.body.asFormUrlEncoded.get("edit").head
      Future(Redirect("/edit/" + id))
    } else {
      val id = request.body.asFormUrlEncoded.get("delete").head
      println("Template of id " + id + " will be removed")
      QuizCreationServiceImpl.deleteTemplateWithEverything(id.toLong) map {
        id =>
          Redirect("/edit")
      }
    }
  }
}
