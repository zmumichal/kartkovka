package controllers

object Dummies {
  case class QuizTemplate(name: String, questions: List[QuestionTemplate])
  case class QuestionTemplate(text: String, answers: List[AnswerTemplate])
  case class AnswerTemplate(text: String, correct: Boolean)
  case class QuizForm(name: String, questions: List[String])

  val dummyTemplate = QuizTemplate("template1",
    List(QuestionTemplate("Question1?", List(
      AnswerTemplate("Answer1", true),
      AnswerTemplate("Answer2", false))),
      QuestionTemplate("Question2?", List(
        AnswerTemplate("Answer3", true),
        AnswerTemplate("Answer4", false)))
    ))

  val dummyTemplate2 = QuizTemplate("template2",
    List(QuestionTemplate("Question1?", List(
      AnswerTemplate("Answer1", true),
      AnswerTemplate("Answer2", false))),
      QuestionTemplate("Question2?", List(
        AnswerTemplate("Answer3", true),
        AnswerTemplate("Answer4", false)))
    ))

  def getTemplateById(id: String): QuizTemplate = {
    if (id.equals(dummyTemplate.name)) dummyTemplate else dummyTemplate2
  }

  def getFormFromObject(template: QuizTemplate): QuizForm = {
    QuizForm(template.name, template.questions.map {
      question =>
        question.text + "\n" + question.answers.map(answer => {
          val prefix = if (answer.correct) "+" else "-"
          prefix + answer.text
        }).reduce(_ + "\n" + _)
    })
  }
}
