package services

import models.{QuestionTemplate, QuizTemplate}

import scala.concurrent.Future

abstract class QuizCreationService {
  def createQuiz(name: String): Future[Long]

  def listQuizzes(): Future[Seq[QuizTemplate]]

  def cloneQuiz(quizTemplate: QuizTemplate): QuizTemplate

  def addQuestionWithAnswers(templateId: Future[Long], source: String): Future[Seq[Long]]

  def updateQuestionWithAnswers(questionTemplate: QuestionTemplate, source: String): QuestionTemplate

  def removeQuestion(questionTemplate: QuestionTemplate): QuizTemplate
}
