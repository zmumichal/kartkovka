package services

import models.{Quiz, QuizInstance, QuizTemplate}
import scala.concurrent.Future

abstract class QuizInstantiationService {
  def createQuiz(quizTemplateId: Long, durationInSeconds: Int, maxNumberOfQuestions: Int, maxNumberOfAnswers: Int, mask: String): String

  def startQuiz(quiz: Quiz): Quiz

  def instantiate(quizId: Long, albumNumber: String, ip: String, sessionUnique: String): Future[QuizInstance]
}
