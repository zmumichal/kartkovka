package services.impl

import controllers.Dummies.QuizForm
import models.{AnswerTemplate, QuestionTemplate, QuizTemplate}
import persistence.dao.{AnswerTemplateDAO, QuestionTemplateDAO, QuizTemplateDAO}
import services.QuizCreationService
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

import scala.util.{Failure, Success}

object QuizCreationServiceImpl extends QuizCreationService {
  def getFormById(templateId: Long): QuizForm = {
    val found = QuizTemplateDAO.findById(templateId)

    QuizForm("myform", List())
  }

  override def createQuiz(name: String): Future[Long] = {
    val template = QuizTemplate(None, name)
    QuizTemplateDAO.insert(template)
  }

  override def listQuizzes(): Future[Seq[QuizTemplate]] = {
    QuizTemplateDAO.listFirst(10)
  }

  override def cloneQuiz(quizTemplate: QuizTemplate): QuizTemplate = ???

  override def removeQuestion(questionTemplate: QuestionTemplate): QuizTemplate = ???

  def updateQuizTemplate(quizTemplateId: Long, newQuestions: List[String]) = {
    // delete all questions and answers for this template and add them anew
    // a more sophisticated method would be too time-consuming
    val questions = QuestionTemplateDAO.findByQuizTemplateId(quizTemplateId)
    questions map {
      realQuestions =>
        realQuestions map {
          question =>
            AnswerTemplateDAO.deleteQuestionsTemplateAnswers(question.id.get) map {
              id =>
                QuestionTemplateDAO.deleteQuizTemplateQuestions(quizTemplateId) map {
                  id =>
                    newQuestions map {
                      addQuestionWithAnswers(Future(quizTemplateId), _)
                    }
                }
            }
        }
    }
  }

  def deleteTemplateWithEverything(quizTemplateId: Long): Future[Int] = {
    val questions = QuestionTemplateDAO.findByQuizTemplateId(quizTemplateId)
    questions map {
      realQuestions =>
        realQuestions map {
          question =>
            AnswerTemplateDAO.deleteQuestionsTemplateAnswers(question.id.get)
        }
    }
    QuestionTemplateDAO.deleteQuizTemplateQuestions(quizTemplateId)
    QuizTemplateDAO.delete(quizTemplateId)
  }

  override def updateQuestionWithAnswers(questionTemplate: QuestionTemplate, source: String): QuestionTemplate = ???

  override def addQuestionWithAnswers(quizTemplateId: Future[Long], source: String) = {
    quizTemplateId map {
      templateId =>
        val parts = source.split("\n")
        val questionTemplate = QuestionTemplate(None, parts(0), templateId)
        val questionTemplateId = QuestionTemplateDAO.insert(questionTemplate)

        questionTemplateId map {
          questionId =>
            AnswerTemplateDAO.batchInsert(parts.tail map { answer =>
              val correct = answer.startsWith("+")
              AnswerTemplate(None, answer.substring(1), correct, questionId)
            })
        }
    } flatMap identity flatMap identity
  }

  def addQuestionsWithAnswers(quizTemplateId: Future[Long], questions: Seq[String]): Future[Seq[Future[Seq[Long]]]] = {
    val ret = quizTemplateId map { id =>
      QuestionTemplateDAO.batchInsert(questions map {
        _.split("\n")(0)
      } map {
        QuestionTemplate(None, _, id)
      }) map { x =>
        x zip (questions map (_.split("\n").tail)) map { element =>
          AnswerTemplateDAO.batchInsert(element._2.map { answer =>
            val correct = answer.startsWith("+")
            AnswerTemplate(None, answer.substring(1), correct, element._1)
          })
        }
      }
    }
    ret flatMap identity
  }
}
