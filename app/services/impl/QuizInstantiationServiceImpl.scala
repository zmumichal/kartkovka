package services.impl

import models._
import org.joda.time.DateTime
import persistence.dao._
import play.api.libs.concurrent.Execution.Implicits._
import services.QuizInstantiationService

import scala.concurrent.Future
import scala.util.Random

object QuizInstantiationServiceImpl extends QuizInstantiationService {
  override def createQuiz(quizTemplateId: Long, durationInSeconds: Int, maxNumberOfQuestions: Int, maxNumberOfAnswers: Int, mask: String): String = {

    val token = Random.alphanumeric.take(10).mkString
    QuizDAO.insert(Quiz(None, new DateTime(), durationInSeconds, mask, token, quizTemplateId))
    token
  }

  override def instantiate(quizId: Long, albumNumber: String, ip: String, sessionUnique: String): Future[QuizInstance] = {
    QuizDAO.findById(quizId).flatMap {
      quiz => {
        val (beforeEnd: Boolean, afterStart: Boolean, ipMatches: Boolean) = ServiceUtil.verifyDatesAndIpMask(ip, quiz)

        (ipMatches, beforeEnd, afterStart) match {
          case (true, true, true) =>
            QuizInstanceDAO.findByQuizIdAndAlbum(quiz.id.get, albumNumber).flatMap {
              case None =>
                QuizInstanceDAO.insert(QuizInstance(None, albumNumber, sessionUnique, 0, false, quiz.id.get)).flatMap {
                  instanceId =>
                    QuestionTemplateDAO.findByQuizTemplateId(quiz.quizTemplateId).flatMap {
                      questionTemplates => Future.sequence(questionTemplates map {
                        questionTemplate => AnswerTemplateDAO.findByQuestionTemplateId(questionTemplate.id.get).flatMap {
                          answerTemplates =>
                            QuestionDAO.insert(Question(None, questionTemplate.question, instanceId)).flatMap(
                              id =>
                                Future.sequence(answerTemplates map {
                                  answerTemplate => AnswerDAO.insert(Answer(None, answerTemplate.answer, answerTemplate.correct, chosen = None, id))
                                })
                            )
                        }
                      })
                    }.flatMap(_ => QuizInstanceDAO.findById(instanceId))
                }
              case Some(instance) => Future(instance)
            }
          case _ =>
            Future(null)
        }
      }
    }
  }

  override def startQuiz(quiz: Quiz): Quiz = ???
}
