package services.impl

import models.{Answer, Question, QuizInstance}
import persistence.dao.{AnswerDAO, QuestionDAO, QuizDAO, QuizInstanceDAO}
import play.api.libs.concurrent.Execution.Implicits._
import services.QuizProgressService

import scala.collection.immutable.Iterable
import scala.concurrent.duration._
import scala.concurrent.{Await, Future}

object QuizProgressServiceImpl extends QuizProgressService {

  override def getQuestions(quizInstanceId: Long): Future[Map[Question, Seq[Answer]]] = {
    QuestionDAO.findByQuizInstanceId(quizInstanceId).map {
      (questions: Seq[Question]) =>
        Map(questions map { a => a -> Await.result(AnswerDAO.findByQuestionId(a.id.get), 10 seconds) }: _*)
    }
  }

  def updateAnswers(instance: QuizInstance, answerIdToChoice: Map[Long, Boolean]): Future[Iterable[Int]] = {
    Future.sequence(answerIdToChoice.map {
      case (answerId, choice) => AnswerDAO.findById(answerId).flatMap { answer =>
        QuestionDAO.findById(answer.questionId).flatMap {
          case Question(_, _, answersQuizInstanceId) => if (answersQuizInstanceId == instance.id.get) {
            AnswerDAO.update(answerId, Answer(answer.id, answer.answer, answer.correct, Some(choice), answer.questionId))
          } else {
            Future(-1)
          }
        }
      }
    })
  }

  def updateScore(quizInstance: QuizInstance): Future[Any] = {
    getQuestions(quizInstance.id.get).map {
      (questions: Map[Question, Seq[Answer]]) => questions.map { case (question, answers) =>
        val correctCount = answers.count {
          answer => (answer.correct, answer.chosen) match {
            case (true, Some(true)) => true
            case _ => false
          }
        }
        val count = answers.count(_.correct)
        correctCount.toDouble / count
      }.sum / questions.keys.size
    }.flatMap(newScore => QuizInstanceDAO.update(quizInstance.id.get, quizInstance.copy(score = newScore)))
  }

  override def updateResults(quizInstanceId: Long, answerIdToChoice: Map[Long, Boolean], sessionUnique: String, ip: String): Future[Boolean] = {
    QuizInstanceDAO.findById(quizInstanceId).flatMap {
      instance =>
        QuizDAO.findById(instance.quizId) flatMap {
          quiz =>
            val (beforeEnd: Boolean, afterStart: Boolean, ipMatches: Boolean) = ServiceUtil.verifyDatesAndIpMask(ip, quiz)
            (ipMatches, beforeEnd, afterStart, instance.sessionUnique.equals(sessionUnique), instance.submitted) match {
              case (true, true, true, true, false) => updateAnswers(instance, answerIdToChoice).flatMap {
                _ => updateScore(instance).flatMap(_ => Future(true))
              }
              case _ => Future(false)
            }
        }
    }
  }

  override def updateResultsAndFinish(quizInstanceId: Long, answerIdToChoice: Map[Long, Boolean], sessionUnique: String, ip: String): Future[Boolean] = {
    updateResults(quizInstanceId, answerIdToChoice, sessionUnique, ip).flatMap {
      case true =>
        QuizInstanceDAO.findById(quizInstanceId).flatMap {
          instance =>
            QuizInstanceDAO.update(quizInstanceId, instance.copy(submitted = true)).flatMap(_ => Future(true))
        }
      case false => Future(false)
    }
  }

}
