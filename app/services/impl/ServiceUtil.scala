package services.impl

import java.net.InetAddress
import java.net.InetAddress.getByName

import io.wasted.util.InetPrefix
import models._
import org.joda.time.DateTime

object ServiceUtil {
  def verifyDatesAndIpMask(ip: String, quiz: Quiz): (Boolean, Boolean, Boolean) = {
    val Array(maskIp, maskLength) = quiz.ipMask.split("/")
    val address: InetAddress = getByName(maskIp)
    val prefix: InetPrefix = io.wasted.util.InetPrefix(address, maskLength.toInt)

    val beforeEnd: Boolean = DateTime.now().isBefore(quiz.date.plusSeconds(quiz.durationInSeconds))
    val afterStart: Boolean = DateTime.now().isAfter(quiz.date)
    val ipMatches: Boolean = prefix.contains(getByName(ip))
    println(ip + " and mask " + maskIp)
    (beforeEnd, afterStart, ipMatches)
  }
}
