package services

import models.{Answer, Question, QuizInstance}

import scala.concurrent.Future

trait QuizProgressService {
  def getQuestions(quizInstanceId: Long): Future[Map[Question, Seq[Answer]]]

  def updateResults(quizInstanceId: Long, answerIdToChoice: Map[Long, Boolean], sessionUnique: String, ip: String): Future[Boolean]

  def updateResultsAndFinish(quizInstanceId: Long, answerIdToChoice: Map[Long, Boolean], sessionUnique: String, ip: String): Future[Boolean]

}
