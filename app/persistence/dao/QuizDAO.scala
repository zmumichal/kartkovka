package persistence.dao

import models.{Quiz, Quizes}
import slick.driver.SQLiteDriver.api._
import slick.lifted.TableQuery

import scala.concurrent.Future


object QuizDAO {
  val entities = TableQuery[Quizes]

  def db: Database = Database.forConfig("db.kartkovka")

  def filterQuery(id: Long): Query[Quizes, Quiz, Seq] =
    for {
      c <- entities if c.id === id
    } yield c

  def filterTokenQuery(token: String): Query[Quizes, Quiz, Seq] =
    for {
      c <- entities if c.token === token
    } yield c

  def findById(id: Long): Future[Quiz] = {
    try db.run(filterQuery(id).result.head)
    finally db.close()
  }

  def findByToken(token: String): Future[Quiz] = {
    try db.run(filterTokenQuery(token).result.head)
    finally db.close()
  }

  def insert(entity: Quiz): Future[Long] =
    try db.run((entities returning entities.map(_.id)) += entity)
    finally db.close()

  def update(id: Long, entity: Quiz): Future[Int] =
    try db.run(filterQuery(id).update(entity))
    finally db.close()

  def delete(id: Long): Future[Int] =
    try db.run(filterQuery(id).delete)
    finally db.close()
}