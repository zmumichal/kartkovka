package persistence.dao

import models.{QuizTemplate, QuizTemplates}
import slick.driver.SQLiteDriver.api._
import slick.lifted.TableQuery

import scala.concurrent.Future


object QuizTemplateDAO {
  val entities = TableQuery[QuizTemplates]

  def db: Database = Database.forConfig("db.kartkovka")

  def filterQuery(id: Long): Query[QuizTemplates, QuizTemplate, Seq] =
    for {
      c <- entities if c.id === id
    } yield c

  def listFirst(n: Int): Future[Seq[QuizTemplate]] = {
    try db.run(entities.take(n).result)
    finally db.close()
  }

  def findById(id: Long): Future[QuizTemplate] = {
    try db.run(filterQuery(id).result.head)
    finally db.close()
  }

  def insert(entity: QuizTemplate): Future[Long] =
    try db.run((entities returning entities.map(_.id)) += entity)
    finally db.close()

  def update(id: Long, entity: QuizTemplate): Future[Int] =
    try db.run(filterQuery(id).update(entity))
    finally db.close()

  def delete(id: Long): Future[Int] =
    try db.run(filterQuery(id).delete)
    finally db.close()
}