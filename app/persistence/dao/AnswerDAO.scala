package persistence.dao

import models.{Answer, Answers}
import slick.driver.SQLiteDriver.api._
import slick.lifted.TableQuery

import scala.concurrent.Future


object AnswerDAO {
  val entities = TableQuery[Answers]

  def db: Database = Database.forConfig("db.kartkovka")

  def filterQuery(id: Long): Query[Answers, Answer, Seq] =
    for {
      c <- entities if c.id === id
    } yield c

  def filterByQuestionQuery(id: Long): Query[Answers, Answer, Seq] =
    for {
      c <- entities if c.questionId === id
    } yield c


  def findById(id: Long): Future[Answer] = {
    try db.run(filterQuery(id).result.head)
    finally db.close()
  }

  def findByQuestionId(id: Long): Future[Seq[Answer]] = {
    try db.run(filterByQuestionQuery(id).result)
    finally db.close()
  }

  def insert(entity: Answer): Future[Long] =
    try db.run((entities returning entities.map(_.id)) += entity)
    finally db.close()

  def update(id: Long, entity: Answer): Future[Int] =
    try db.run(filterQuery(id).update(entity))
    finally db.close()

  def delete(id: Long): Future[Int] =
    try db.run(filterQuery(id).delete)
    finally db.close()
}