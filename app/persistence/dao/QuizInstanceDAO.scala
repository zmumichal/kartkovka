package persistence.dao

import models.{QuizInstance, QuizInstances}
import slick.driver.SQLiteDriver.api._
import slick.lifted.TableQuery

import scala.concurrent.Future

object QuizInstanceDAO {
  val entities = TableQuery[QuizInstances]

  def db: Database = Database.forConfig("db.kartkovka")

  def filterQuery(id: Long): Query[QuizInstances, QuizInstance, Seq] =
    for {
      c <- entities if c.id === id
    } yield c

  def getNLatest(n: Int): Future[Seq[QuizInstance]] = {
    try db.run(entities.take(n).result)
    finally db.close()
  }

  def filterQuizQuery(id: Long): Query[QuizInstances, QuizInstance, Seq] =
    for {
      c <- entities if c.quizId === id
    } yield c

  def filterByQuizIdAndAlbumQuery(id: Long, album: String): Query[QuizInstances, QuizInstance, Seq] =
    for {
      c <- entities if c.quizId === id && c.albumNumber === album
    } yield c

  def findById(id: Long): Future[QuizInstance] =
    try db.run(filterQuery(id).result.head)
    finally db.close()

  def findByQuizId(id: Long): Future[QuizInstance] =
    try db.run(filterQuizQuery(id).result.head)
    finally db.close()

  def findByQuizIdAndAlbum(id: Long, album: String): Future[Option[QuizInstance]] =
    try db.run(filterByQuizIdAndAlbumQuery(id, album).result.headOption)
    finally db.close()

  def insert(entity: QuizInstance): Future[Long] =
    try db.run((entities returning entities.map(_.id)) += entity)
    finally db.close()

  def update(id: Long, entity: QuizInstance): Future[Int] =
    try db.run(filterQuery(id).update(entity))
    finally db.close()

  def delete(id: Long): Future[Int] =
    try db.run(filterQuery(id).delete)
    finally db.close()
}