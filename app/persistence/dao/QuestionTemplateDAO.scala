package persistence.dao

import models.{QuizTemplate, QuestionTemplate, QuestionTemplates}
import slick.driver.SQLiteDriver.api._
import slick.lifted.TableQuery

import scala.concurrent.Future


object QuestionTemplateDAO {
  val entities = TableQuery[QuestionTemplates]

  def db: Database = Database.forConfig("db.kartkovka")

  def filterQuery(id: Long): Query[QuestionTemplates, QuestionTemplate, Seq] =
    for {
      c <- entities if c.id === id
    } yield c

  def filterByQuizTemplateQuery(id: Long): Query[QuestionTemplates, QuestionTemplate, Seq] =
    for {
      c <- entities if c.quizTemplateId === id
    } yield c

  def findById(id: Long): Future[QuestionTemplate] = {
    try db.run(filterQuery(id).result.head)
    finally db.close()
  }

  def findByQuizTemplateId(id: Long): Future[Seq[QuestionTemplate]] = {
    try db.run(filterByQuizTemplateQuery(id).result)
    finally db.close()
  }

  def deleteQuizTemplateQuestions(id: Long) = {
    try db.run(filterByQuizTemplateQuery(id).delete)
    finally db.close()
  }

  def batchInsert(answers: Seq[QuestionTemplate]): Future[Seq[Long]] =
    try db.run((entities returning entities.map(_.id)) ++= answers)
    finally db.close()

  def insert(entity: QuestionTemplate): Future[Long] =
    try db.run((entities returning entities.map(_.id)) += entity)
    finally db.close()

  def update(id: Long, entity: QuestionTemplate): Future[Int] =
    try db.run(filterQuery(id).update(entity))
    finally db.close()

  def delete(id: Long): Future[Int] =
    try db.run(filterQuery(id).delete)
    finally db.close()
}