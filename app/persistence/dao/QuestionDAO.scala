package persistence.dao

import models.{Question, Questions}
import slick.driver.SQLiteDriver.api._
import slick.lifted.TableQuery

import scala.concurrent.Future


object QuestionDAO {
  val entities = TableQuery[Questions]
  def db: Database = Database.forConfig("db.kartkovka")

  def filterQuery(id: Long): Query[Questions, Question, Seq] =
    for {
      c <- entities if c.id === id
    } yield c

  def filterByQuizInstanceQuery(id: Long): Query[Questions, Question, Seq] =
    for {
      c <- entities if c.quizInstanceId === id
    } yield c

  def findById(id: Long): Future[Question] = {
    try db.run(filterQuery(id).result.head)
    finally db.close()
  }

  def findByQuizInstanceId(id: Long): Future[Seq[Question]] = {
    try db.run(filterByQuizInstanceQuery(id).result)
    finally db.close()
  }

  def insert(entity: Question): Future[Long] =
    try db.run((entities returning entities.map(_.id)) += entity)
    finally db.close()

  def update(id: Long, entity: Question): Future[Int] =
    try db.run(filterQuery(id).update(entity))
    finally db.close()

  def delete(id: Long): Future[Int] =
    try db.run(filterQuery(id).delete)
    finally db.close()
}