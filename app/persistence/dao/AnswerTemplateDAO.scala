package persistence.dao

import models.{AnswerTemplate, AnswerTemplates}
import slick.driver.SQLiteDriver.api._
import slick.lifted.TableQuery

import scala.concurrent.Future


object AnswerTemplateDAO {
  val entities = TableQuery[AnswerTemplates]

  def db: Database = Database.forConfig("db.kartkovka")

  def filterQuery(id: Long): Query[AnswerTemplates, AnswerTemplate, Seq] =
    for {
      c <- entities if c.id === id
    } yield c

  def filterQuestionTemplateQuery(id: Long): Query[AnswerTemplates, AnswerTemplate, Seq] =
    for {
      c <- entities if c.questionTemplateId === id
    } yield c

  def findById(id: Long): Future[AnswerTemplate] = {
    try db.run(filterQuery(id).result.head)
    finally db.close()
  }

  def findByQuestionTemplateId(id: Long): Future[Seq[AnswerTemplate]] = {
    try db.run(filterQuestionTemplateQuery(id).result)
    finally db.close()
  }

  def deleteQuestionsTemplateAnswers(id: Long) = {
    try db.run(filterQuestionTemplateQuery(id).delete)
    finally db.close()
  }

  def batchInsert(answers: Seq[AnswerTemplate]): Future[Seq[Long]] =
    try db.run((entities returning entities.map(_.id)) ++= answers)
    finally db.close()

  def insert(entity: AnswerTemplate): Future[Long] =
    try db.run((entities returning entities.map(_.id)) += entity)
    finally db.close()

  def update(id: Long, entity: AnswerTemplate): Future[Int] =
    try db.run(filterQuery(id).update(entity))
    finally db.close()

  def delete(id: Long): Future[Int] =
    try db.run(filterQuery(id).delete)
    finally db.close()
}