package persistence

import models._
import slick.driver.SQLiteDriver.api._

import scala.concurrent.ExecutionContext.Implicits.global

trait CreateSchema {
  def createSchema() = {
    val setup = DBIO.seq({
      val tables = List(
        TableQuery[Answers],
        TableQuery[AnswerTemplates],
        TableQuery[Questions],
        TableQuery[QuestionTemplates],
        TableQuery[Quizes],
        TableQuery[QuizTemplates],
        TableQuery[QuizInstances]
      )

      tables.map(_.schema).reduce((a, b) => a ++ b).create
    })

    val db = Database.forConfig("db.kartkovka")
    db.run(setup).onComplete(_ => db.close())

  }
}