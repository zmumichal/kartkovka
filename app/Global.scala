import models._
import org.joda.time.DateTime
import persistence.CreateSchema
import persistence.dao._
import play.api._
import play.api.libs.concurrent.Execution.Implicits._

object Global extends GlobalSettings with CreateSchema {

  override def onStart(app: Application): Unit = {

    Logger.info("Application has started")
    Logger.info("Creating schema if not created...")
    this.createSchema()

    Logger.info("Inserting data...")
    for {
      templateId <- QuizTemplateDAO.insert(QuizTemplate(None, "test-template"))
      quizId <- QuizDAO.insert(Quiz(None, DateTime.now(), 10000, "0.0.0.0/0", "test-quiz-" + templateId, templateId))

      question1Id <- QuestionTemplateDAO.insert(QuestionTemplate(None, "test-question-1", templateId))
      answer1Id <- AnswerTemplateDAO.insert(AnswerTemplate(None, "test-question-1-answer-1-correct", true, question1Id))
      answer2Id <- AnswerTemplateDAO.insert(AnswerTemplate(None, "test-question-1-answer-2-correct", true, question1Id))

      question2Id <- QuestionTemplateDAO.insert(QuestionTemplate(None, "test-question-2", templateId))
      answer3Id <- AnswerTemplateDAO.insert(AnswerTemplate(None, "test-question-2-answer-1-not-correct", false, question2Id))
      answer4Id <- AnswerTemplateDAO.insert(AnswerTemplate(None, "test-question-2-answer-2-correct", true, question2Id))

    } yield None
  }
}
