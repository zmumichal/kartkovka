package models

import slick.driver.SQLiteDriver.api._

case class AnswerTemplate(id: Option[Long], answer: String, correct: Boolean, questionTemplateId: Long)

class AnswerTemplates(tag: Tag) extends Table[AnswerTemplate](tag, "ANSWER_TEMPLATES") {
  def id = column[Long]("ID", O.PrimaryKey, O.AutoInc)

  def answer = column[String]("ANSWER")

  def correct = column[Boolean]("CORRECT")

  def questionTemplateId = column[Long]("QUESTION_TEMPLATE_ID")

  def questionTemplate = foreignKey("QUESTION_TEMPLATE_FK", questionTemplateId, TableQuery[QuestionTemplates])(_.id)

  def * = (id.?, answer, correct, questionTemplateId) <>((AnswerTemplate.apply _).tupled, AnswerTemplate.unapply)
}


