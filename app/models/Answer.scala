package models

import slick.driver.SQLiteDriver.api._

case class Answer(id: Option[Long], answer: String, correct: Boolean, chosen: Option[Boolean], questionId: Long)

class Answers(tag: Tag) extends Table[Answer](tag, "ANSWERS") {
  def id = column[Long]("ID", O.PrimaryKey, O.AutoInc)

  def answer = column[String]("ANSWER")

  def correct = column[Boolean]("CORRECT")

  def chosen = column[Option[Boolean]]("CHOSEN")

  def questionId = column[Long]("QUESTION_ID")

  def question = foreignKey("QUESTION_FK", questionId, TableQuery[Questions])(_.id)

  def * = (id.?, answer, correct, chosen, questionId) <>((Answer.apply _).tupled, Answer.unapply)
}


