package models

import slick.driver.SQLiteDriver.api._

case class Question(id: Option[Long], question: String, quizInstanceId: Long)

class Questions(tag: Tag) extends Table[Question](tag, "QUESTIONS") {
  def id = column[Long]("ID", O.PrimaryKey, O.AutoInc)

  def question = column[String]("QUESTION")

  def quizInstanceId = column[Long]("QUIZ_INSTANCE_ID")

  def quizInstance = foreignKey("QUIZ_INSTANCE_FK", quizInstanceId, TableQuery[QuizInstances])(_.id)

  def * = (id.?, question, quizInstanceId) <>((Question.apply _).tupled, Question.unapply)
}


