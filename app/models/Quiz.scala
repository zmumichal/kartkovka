package models

import com.github.tototoshi.slick.SQLiteJodaSupport._
import org.joda.time.DateTime
import slick.driver.SQLiteDriver.api._

case class Quiz(id: Option[Long], date: DateTime, durationInSeconds: Int, ipMask: String, token: String, quizTemplateId: Long)

class Quizes(tag: Tag) extends Table[Quiz](tag, "QUIZES") {
  def id = column[Long]("ID", O.PrimaryKey, O.AutoInc)

  def date = column[DateTime]("DATE")

  def durationInSeconds = column[Int]("DURATION_IN_SECONDS")

  def token = column[String]("TOKEN")

  def ipMask = column[String]("IP_MASK")

  def quizTemplateId = column[Long]("QUIZ_TEMPLATE_ID")

  def quizTemplate = foreignKey("QUIZ_TEMPLATE_FK", quizTemplateId, TableQuery[QuizTemplates])(_.id)

  def * = (id.?, date, durationInSeconds, ipMask, token, quizTemplateId) <>((Quiz.apply _).tupled, Quiz.unapply)
}
