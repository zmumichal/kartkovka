package models

import slick.driver.SQLiteDriver.api._

case class QuestionTemplate(id: Option[Long], question: String, quizTemplateId: Long)

class QuestionTemplates(tag: Tag) extends Table[QuestionTemplate](tag, "QUESTION_TEMPLATES") {
  def id = column[Long]("ID", O.PrimaryKey, O.AutoInc)

  def question = column[String]("QUESTION")

  def quizTemplateId = column[Long]("QUIZ_TEMPLATE_ID")

  def quizTemplate = foreignKey("QUIZ_TEMPLATE_FK", quizTemplateId, TableQuery[QuizTemplates])(_.id)

  def * = (id.?, question, quizTemplateId) <>((QuestionTemplate.apply _).tupled, QuestionTemplate.unapply)
}


