package models

import slick.driver.SQLiteDriver.api._

case class QuizTemplate(id: Option[Long], name: String)

class QuizTemplates(tag: Tag) extends Table[QuizTemplate](tag, "QUIZ_TEMPLATES") {
  def id = column[Long]("ID", O.PrimaryKey, O.AutoInc)

  def name = column[String]("NAME")

  def * = (id.?, name) <> ((QuizTemplate.apply _).tupled, QuizTemplate.unapply)
}
