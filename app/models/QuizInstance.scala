package models

import slick.driver.SQLiteDriver.api._

case class QuizInstance(id: Option[Long], albumNumber: String, sessionUnique: String, score: Double, submitted: Boolean, quizId: Long)

class QuizInstances(tag: Tag) extends Table[QuizInstance](tag, "QUIZ_INSTANCES") {
  def id = column[Long]("ID", O.PrimaryKey, O.AutoInc)

  def albumNumber = column[String]("ALBUM_NUMBER")

  def score = column[Double]("SCORE")

  def submitted = column[Boolean]("SUBMITTED")

  def sessionUnique = column[String]("SESSION_UNIQUE")

  def quizId = column[Long]("QUIZ_ID")

  def quiz = foreignKey("QUIZ_FK", quizId, TableQuery[Quizes])(_.id)

  def * = (id.?, albumNumber, sessionUnique, score, submitted, quizId) <>((QuizInstance.apply _).tupled, QuizInstance.unapply)
}


