package services

import models._
import org.joda.time.DateTime
import org.scalatest._
import persistence.dao._
import services.impl.{QuizInstantiationServiceImpl, QuizProgressServiceImpl}

import scala.concurrent.Await
import scala.concurrent.duration._

class QuizInstantiationServiceSpec extends FlatSpec with Matchers {
  "QuizInstantiationService" should "create quiz with maximal amount of questions not exceeded" in {}
  "QuizInstantiationService" should "create quiz with maximal amount of answers not exceeded" in {}
  "QuizInstantiationService" should "create quiz successfully if less than requested questions available" in {}
  "QuizInstantiationService" should "create quiz successfully if less than requested answers available" in {}
  "QuizInstantiationService" should "create quiz with start date not set" in {}
  "QuizInstantiationService" should "update start date of quiz on starting the quiz" in {}

  "QuizInstantiationService" should "instantiate quiz only if not finished" in {
    val templateId = Await.result(QuizTemplateDAO.insert(QuizTemplate(None, "test-template")), 10 seconds)
    val quizId = Await.result(QuizDAO.insert(Quiz(None, DateTime.now(), 10000, "1.2.176.0/20", "test-quiz", templateId)), 10 seconds)
    val finishedQuizId = Await.result(QuizDAO.insert(Quiz(None, DateTime.now().minusDays(1), 10000, "1.2.176.0/20", "test-quiz", templateId)), 10 seconds)

    Await.result(QuizInstantiationServiceImpl.instantiate(quizId, "250250", "1.2.191.20", "uuid"), 10 seconds) shouldNot be(null)
    Await.result(QuizInstantiationServiceImpl.instantiate(finishedQuizId, "250250", "1.2.191.20", "uuid"), 10 seconds) should be(null)
  }

  "QuizInstantiationService" should "instantiate quiz only if ip address matches mask" in {
    val templateId = Await.result(QuizTemplateDAO.insert(QuizTemplate(None, "test-template")), 10 seconds)
    val quizId = Await.result(QuizDAO.insert(Quiz(None, DateTime.now(), 10000, "1.2.176.0/20", "test-quiz", templateId)), 10 seconds)

    Await.result(QuizInstantiationServiceImpl.instantiate(quizId, "250250", "1.2.191.20", "uuid"), 10 seconds) shouldNot be(null)
    Await.result(QuizInstantiationServiceImpl.instantiate(quizId, "250250", "1.2.192.0", "uuid"), 10 seconds) should be(null)
    Await.result(QuizInstantiationServiceImpl.instantiate(quizId, "250250", "1.2.172.0", "uuid"), 10 seconds) should be(null)
  }

  "QuizInstantiationService" should "return matching instance if such exists" in {
    val templateId = Await.result(QuizTemplateDAO.insert(QuizTemplate(None, "test-template")), 10 seconds)
    val quizId = Await.result(QuizDAO.insert(Quiz(None, DateTime.now(), 10000, "0.0.0.0/0", "test-quiz", templateId)), 10 seconds)

    val instanceId = Await.result(QuizInstanceDAO.insert(QuizInstance(None, "ALBUM", "uuid", 0, false, quizId)), 10 seconds)
    Await.result(QuizInstantiationServiceImpl.instantiate(quizId, "ALBUM", "1.2.191.20", "uuid"), 10 seconds) should have(
      'id(Some(instanceId))
    )
  }

  "QuizInstantiationService" should "return matching instance only if not finished" in {
    val templateId = Await.result(QuizTemplateDAO.insert(QuizTemplate(None, "test-template")), 10 seconds)
    val quizId = Await.result(QuizDAO.insert(Quiz(None, DateTime.now(), 10000, "1.2.176.0/20", "test-quiz", templateId)), 10 seconds)
    val finishedQuizId = Await.result(QuizDAO.insert(Quiz(None, DateTime.now().minusDays(1), 10000, "1.2.176.0/20", "test-quiz", templateId)), 10 seconds)

    val instanceId = Await.result(QuizInstanceDAO.insert(QuizInstance(None, "ALBUM", "uuid", 0, false, quizId)), 10 seconds)
    Await.result(QuizInstantiationServiceImpl.instantiate(quizId, "ALBUM", "1.2.191.20", "uuid"), 10 seconds) shouldNot be(null)

    val finishedInstanceId = Await.result(QuizInstanceDAO.insert(QuizInstance(None, "ALBUM", "uuid", 0, false, finishedQuizId)), 10 seconds)
    Await.result(QuizInstantiationServiceImpl.instantiate(finishedQuizId, "ALBUM", "1.2.191.20", "uuid"), 10 seconds) should be(null)
  }

  "QuizInstantiationService" should "set unique session on instantiate" in {
    val templateId = Await.result(QuizTemplateDAO.insert(QuizTemplate(None, "test-template")), 10 seconds)
    val quizId = Await.result(QuizDAO.insert(Quiz(None, DateTime.now(), 10000, "1.2.176.0/20", "test-quiz", templateId)), 10 seconds)

    Await.result(QuizInstantiationServiceImpl.instantiate(quizId, "250250", "1.2.191.20", "uuid"), 10 seconds) should have(
      'sessionUnique("uuid")
    )
  }

  "QuizInstantiationService" should "instantiate questions and answers on quiz instantiation" in {
    val templateId = Await.result(QuizTemplateDAO.insert(QuizTemplate(None, "test-template")), 10 seconds)
    val quizId = Await.result(QuizDAO.insert(Quiz(None, DateTime.now(), 10000, "0.0.0.0/0", "test-quiz-" + templateId, templateId)), 10 seconds)

    val question1Id = Await.result(QuestionTemplateDAO.insert(QuestionTemplate(None, "test-question-1", templateId)), 10 seconds)
    val answer1Id = Await.result(AnswerTemplateDAO.insert(AnswerTemplate(None, "test-question-1-answer-1-correct", true, question1Id)), 10 seconds)
    val answer2Id = Await.result(AnswerTemplateDAO.insert(AnswerTemplate(None, "test-question-1-answer-2-correct", true, question1Id)), 10 seconds)

    val question2Id = Await.result(QuestionTemplateDAO.insert(QuestionTemplate(None, "test-question-2", templateId)), 10 seconds)
    val answer3Id = Await.result(AnswerTemplateDAO.insert(AnswerTemplate(None, "test-question-2-answer-1-not-correct", false, question2Id)), 10 seconds)
    val answer4Id = Await.result(AnswerTemplateDAO.insert(AnswerTemplate(None, "test-question-2-answer-2-correct", true, question2Id)), 10 seconds)

    //when
    val instance: QuizInstance = Await.result(QuizInstantiationServiceImpl.instantiate(quizId, "250250", "1.2.191.20", "uuid"), 10 seconds)

    //then
    val map: Map[Question, Seq[Answer]] = Await.result(QuizProgressServiceImpl.getQuestions(instance.id.get), 10 seconds)

    val questions: List[Question] = map.keys.toList.sortWith(_.question < _.question)
    questions should have length 2
    questions.head should have(
      'quizInstanceId(instance.id.get),
      'question("test-question-1")
    )
    questions.last should have(
      'quizInstanceId(instance.id.get),
      'question("test-question-2")
    )

    val answers1: List[Answer] = map.get(questions.head).get.toList.sortWith(_.answer < _.answer)
    answers1.head should have(
      'questionId(questions.head.id.get),
      'answer("test-question-1-answer-1-correct")
    )
    answers1.last should have(
      'questionId(questions.head.id.get),
      'answer("test-question-1-answer-2-correct")
    )

    val answers2: List[Answer] = map.get(questions.last).get.toList.sortWith(_.answer < _.answer)
    answers2.head should have(
      'questionId(questions.last.id.get),
      'answer("test-question-2-answer-1-not-correct")
    )
    answers2.last should have(
      'questionId(questions.last.id.get),
      'answer("test-question-2-answer-2-correct")
    )
  }
}