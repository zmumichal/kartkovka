package services

import models._
import org.joda.time.DateTime
import org.scalatest._
import persistence.dao._
import services.impl.QuizProgressServiceImpl

import scala.concurrent.Await
import scala.concurrent.duration._

class QuizProgressServiceSpec extends FlatSpec with Matchers {
  "QuizProgressService" should "get question and answers for instance" in {
    //given
    val templateId = Await.result(QuizTemplateDAO.insert(QuizTemplate(None, "test-template")), 10 seconds)
    val quizId = Await.result(QuizDAO.insert(Quiz(None, DateTime.now(), 10000, "0.0.0.0/0", "test-quiz", templateId)), 10 seconds)
    val instanceId = Await.result(QuizInstanceDAO.insert(QuizInstance(None, "250042", "", 0, false, quizId)), 10 seconds)

    val question1Id = Await.result(QuestionDAO.insert(Question(None, "test-question-1", instanceId)), 10 seconds)
    val answer1Id = Await.result(AnswerDAO.insert(Answer(None, "test-question-1-answer-1-correct", true, Some(false), question1Id)), 10 seconds)
    val answer2Id = Await.result(AnswerDAO.insert(Answer(None, "test-question-1-answer-2-not-correct", false, Some(false), question1Id)), 10 seconds)

    val question2Id = Await.result(QuestionDAO.insert(Question(None, "test-question-2", instanceId)), 10 seconds)
    val answer3Id = Await.result(AnswerDAO.insert(Answer(None, "test-question-2-answer-1-correct", true, Some(false), question2Id)), 10 seconds)
    val answer4Id = Await.result(AnswerDAO.insert(Answer(None, "test-question-2-answer-2-not-correct", false, Some(false), question2Id)), 10 seconds)

    val question1: Question = Await.result(QuestionDAO.findById(question1Id), 10 seconds)
    val question2: Question = Await.result(QuestionDAO.findById(question2Id), 10 seconds)
    val answer1: Answer = Await.result(AnswerDAO.findById(answer1Id), 10 seconds)
    val answer2: Answer = Await.result(AnswerDAO.findById(answer2Id), 10 seconds)
    val answer3: Answer = Await.result(AnswerDAO.findById(answer3Id), 10 seconds)
    val answer4: Answer = Await.result(AnswerDAO.findById(answer4Id), 10 seconds)

    //when
    val map: Map[Question, Seq[Answer]] = Await.result(QuizProgressServiceImpl.getQuestions(instanceId), 10 seconds)

    //then
    map should contain key question1
    map should contain key question2
    map.get(question1).get should contain(answer1)
    map.get(question1).get should contain(answer2)
    map.get(question2).get should contain(answer3)
    map.get(question2).get should contain(answer4)
  }

  "QuizProgressService" should "update score according to answers provided" in {
    //given
    val templateId = Await.result(QuizTemplateDAO.insert(QuizTemplate(None, "test-template")), 10 seconds)
    val quizId = Await.result(QuizDAO.insert(Quiz(None, DateTime.now(), 10000, "0.0.0.0/0", "test-quiz", templateId)), 10 seconds)
    val instanceId = Await.result(QuizInstanceDAO.insert(QuizInstance(None, "250042", "uuid", 0, false, quizId)), 10 seconds)

    val question1Id = Await.result(QuestionDAO.insert(Question(None, "test-question-1", instanceId)), 10 seconds)
    val answer1Id = Await.result(AnswerDAO.insert(Answer(None, "test-question-1-answer-1-correct", true, Some(false), question1Id)), 10 seconds)
    val answer2Id = Await.result(AnswerDAO.insert(Answer(None, "test-question-1-answer-2-not-correct", false, Some(false), question1Id)), 10 seconds)

    val question2Id = Await.result(QuestionDAO.insert(Question(None, "test-question-2", instanceId)), 10 seconds)
    val answer3Id = Await.result(AnswerDAO.insert(Answer(None, "test-question-2-answer-1-correct", true, Some(false), question2Id)), 10 seconds)
    val answer4Id = Await.result(AnswerDAO.insert(Answer(None, "test-question-2-answer-2-not-correct", false, Some(false), question2Id)), 10 seconds)

    val question1: Question = Await.result(QuestionDAO.findById(question1Id), 10 seconds)
    val question2: Question = Await.result(QuestionDAO.findById(question2Id), 10 seconds)
    val answer1: Answer = Await.result(AnswerDAO.findById(answer1Id), 10 seconds)
    val answer2: Answer = Await.result(AnswerDAO.findById(answer2Id), 10 seconds)
    val answer3: Answer = Await.result(AnswerDAO.findById(answer3Id), 10 seconds)
    val answer4: Answer = Await.result(AnswerDAO.findById(answer4Id), 10 seconds)

    //when
    Await.result(QuizProgressServiceImpl.updateResults(instanceId, Map(
      (answer1.id.get, true),
      (answer2.id.get, true),
      (answer3.id.get, true),
      (answer4.id.get, false)
    ), "uuid", "127.0.0.1"), 10 seconds)

    //then
    val instance: QuizInstance = Await.result(QuizInstanceDAO.findById(instanceId), 10 seconds)
    instance should have(
      'score(1)
    )
  }

  "QuizProgressService" should "update completely overriding previous answers" in {
    //given
    val templateId = Await.result(QuizTemplateDAO.insert(QuizTemplate(None, "test-template")), 10 seconds)
    val quizId = Await.result(QuizDAO.insert(Quiz(None, DateTime.now(), 10000, "0.0.0.0/0", "test-quiz", templateId)), 10 seconds)
    val instanceId = Await.result(QuizInstanceDAO.insert(QuizInstance(None, "250042", "uuid", 0, false, quizId)), 10 seconds)

    val question1Id = Await.result(QuestionDAO.insert(Question(None, "test-question-1", instanceId)), 10 seconds)
    val answer1Id = Await.result(AnswerDAO.insert(Answer(None, "test-question-1-answer-1-correct", true, Some(false), question1Id)), 10 seconds)
    val answer2Id = Await.result(AnswerDAO.insert(Answer(None, "test-question-1-answer-2-not-correct", false, Some(false), question1Id)), 10 seconds)

    val question2Id = Await.result(QuestionDAO.insert(Question(None, "test-question-2", instanceId)), 10 seconds)
    val answer3Id = Await.result(AnswerDAO.insert(Answer(None, "test-question-2-answer-1-correct", true, Some(false), question2Id)), 10 seconds)
    val answer4Id = Await.result(AnswerDAO.insert(Answer(None, "test-question-2-answer-2-not-correct", false, Some(false), question2Id)), 10 seconds)

    val question1: Question = Await.result(QuestionDAO.findById(question1Id), 10 seconds)
    val question2: Question = Await.result(QuestionDAO.findById(question2Id), 10 seconds)
    val answer1: Answer = Await.result(AnswerDAO.findById(answer1Id), 10 seconds)
    val answer2: Answer = Await.result(AnswerDAO.findById(answer2Id), 10 seconds)
    val answer3: Answer = Await.result(AnswerDAO.findById(answer3Id), 10 seconds)
    val answer4: Answer = Await.result(AnswerDAO.findById(answer4Id), 10 seconds)

    //when
    Await.result(QuizProgressServiceImpl.updateResults(instanceId, Map(
      (answer1.id.get, true),
      (answer2.id.get, true),
      (answer3.id.get, true),
      (answer4.id.get, true)
    ), "uuid", "127.0.0.1"), 10 seconds)

    //then
    Await.result(AnswerDAO.findById(answer1Id), 10 seconds) should have(
      'chosen(Some(true))
    )
    Await.result(AnswerDAO.findById(answer2Id), 10 seconds) should have(
      'chosen(Some(true))
    )
    Await.result(AnswerDAO.findById(answer3Id), 10 seconds) should have(
      'chosen(Some(true))
    )
    Await.result(AnswerDAO.findById(answer4Id), 10 seconds) should have(
      'chosen(Some(true))
    )
  }
  "QuizProgressService" should "validate answers belong to questions in quiz" in {
    //given
    val templateId = Await.result(QuizTemplateDAO.insert(QuizTemplate(None, "test-template")), 10 seconds)
    val quiz1Id = Await.result(QuizDAO.insert(Quiz(None, DateTime.now(), 10000, "0.0.0.0/0", "test-quiz", templateId)), 10 seconds)
    val quiz2Id = Await.result(QuizDAO.insert(Quiz(None, DateTime.now(), 10000, "0.0.0.0/0", "test-quiz", templateId)), 10 seconds)
    val instance1Id = Await.result(QuizInstanceDAO.insert(QuizInstance(None, "250042", "uuid", 0, false, quiz1Id)), 10 seconds)
    val instance2Id = Await.result(QuizInstanceDAO.insert(QuizInstance(None, "250042", "uuid", 0, false, quiz2Id)), 10 seconds)

    val question1Id = Await.result(QuestionDAO.insert(Question(None, "test-question-1", instance1Id)), 10 seconds)
    val answer1Id = Await.result(AnswerDAO.insert(Answer(None, "test-question-1-answer-1-correct", true, Some(false), question1Id)), 10 seconds)
    val answer2Id = Await.result(AnswerDAO.insert(Answer(None, "test-question-1-answer-2-not-correct", false, Some(false), question1Id)), 10 seconds)

    val question2Id = Await.result(QuestionDAO.insert(Question(None, "test-question-2", instance2Id)), 10 seconds)
    val answer3Id = Await.result(AnswerDAO.insert(Answer(None, "test-question-2-answer-1-correct", true, Some(false), question2Id)), 10 seconds)
    val answer4Id = Await.result(AnswerDAO.insert(Answer(None, "test-question-2-answer-2-not-correct", false, Some(false), question2Id)), 10 seconds)

    val question1: Question = Await.result(QuestionDAO.findById(question1Id), 10 seconds)
    val question2: Question = Await.result(QuestionDAO.findById(question2Id), 10 seconds)
    val answer1: Answer = Await.result(AnswerDAO.findById(answer1Id), 10 seconds)
    val answer2: Answer = Await.result(AnswerDAO.findById(answer2Id), 10 seconds)
    val answer3: Answer = Await.result(AnswerDAO.findById(answer3Id), 10 seconds)
    val answer4: Answer = Await.result(AnswerDAO.findById(answer4Id), 10 seconds)

    //when
    Await.result(QuizProgressServiceImpl.updateResults(instance1Id, Map(
      (answer1.id.get, true),
      (answer2.id.get, true),
      (answer3.id.get, true),
      (answer4.id.get, true)
    ), "uuid", "127.0.0.1"), 10 seconds)

    //then
    Await.result(AnswerDAO.findById(answer1Id), 10 seconds) should have(
      'chosen(Some(true))
    )
    Await.result(AnswerDAO.findById(answer2Id), 10 seconds) should have(
      'chosen(Some(true))
    )
    Await.result(AnswerDAO.findById(answer3Id), 10 seconds) should have(
      'chosen(Some(false))
    )
    Await.result(AnswerDAO.findById(answer4Id), 10 seconds) should have(
      'chosen(Some(false))
    )
  }
  "QuizProgressService" should "validate session unique token" in {
    //given
    val templateId = Await.result(QuizTemplateDAO.insert(QuizTemplate(None, "test-template")), 10 seconds)
    val quizId = Await.result(QuizDAO.insert(Quiz(None, DateTime.now(), 10000, "0.0.0.0/0", "test-quiz", templateId)), 10 seconds)
    val instanceId = Await.result(QuizInstanceDAO.insert(QuizInstance(None, "250042", "uuid", 0, false, quizId)), 10 seconds)

    val question1Id = Await.result(QuestionDAO.insert(Question(None, "test-question-1", instanceId)), 10 seconds)
    val answer1Id = Await.result(AnswerDAO.insert(Answer(None, "test-question-1-answer-1-correct", true, Some(false), question1Id)), 10 seconds)
    val answer2Id = Await.result(AnswerDAO.insert(Answer(None, "test-question-1-answer-2-not-correct", false, Some(false), question1Id)), 10 seconds)

    val question2Id = Await.result(QuestionDAO.insert(Question(None, "test-question-2", instanceId)), 10 seconds)
    val answer3Id = Await.result(AnswerDAO.insert(Answer(None, "test-question-2-answer-1-correct", true, Some(false), question2Id)), 10 seconds)
    val answer4Id = Await.result(AnswerDAO.insert(Answer(None, "test-question-2-answer-2-not-correct", false, Some(false), question2Id)), 10 seconds)

    val question1: Question = Await.result(QuestionDAO.findById(question1Id), 10 seconds)
    val question2: Question = Await.result(QuestionDAO.findById(question2Id), 10 seconds)
    val answer1: Answer = Await.result(AnswerDAO.findById(answer1Id), 10 seconds)
    val answer2: Answer = Await.result(AnswerDAO.findById(answer2Id), 10 seconds)
    val answer3: Answer = Await.result(AnswerDAO.findById(answer3Id), 10 seconds)
    val answer4: Answer = Await.result(AnswerDAO.findById(answer4Id), 10 seconds)

    //when
    Await.result(QuizProgressServiceImpl.updateResults(instanceId, Map(
      (answer1.id.get, true),
      (answer2.id.get, true),
      (answer3.id.get, true),
      (answer4.id.get, false)
    ), "uuid-OTHER", "127.0.0.1"), 10 seconds)

    //then
    val instance: QuizInstance = Await.result(QuizInstanceDAO.findById(instanceId), 10 seconds)
    instance should have(
      'score(0)
    )
  }
  "QuizProgressService" should "not allow changes after quiz deadline" in {
    // given
    val templateId = Await.result(QuizTemplateDAO.insert(QuizTemplate(None, "test-template")), 10 seconds)
    val quizId = Await.result(QuizDAO.insert(Quiz(None, DateTime.now().minusDays(1), 10000, "0.0.0.0/0", "test-quiz", templateId)), 10 seconds)
    val instanceId = Await.result(QuizInstanceDAO.insert(QuizInstance(None, "250042", "uuid", 0, false, quizId)), 10 seconds)

    val question1Id = Await.result(QuestionDAO.insert(Question(None, "test-question-1", instanceId)), 10 seconds)
    val answer1Id = Await.result(AnswerDAO.insert(Answer(None, "test-question-1-answer-1-correct", true, Some(false), question1Id)), 10 seconds)
    val answer2Id = Await.result(AnswerDAO.insert(Answer(None, "test-question-1-answer-2-not-correct", false, Some(false), question1Id)), 10 seconds)

    val question2Id = Await.result(QuestionDAO.insert(Question(None, "test-question-2", instanceId)), 10 seconds)
    val answer3Id = Await.result(AnswerDAO.insert(Answer(None, "test-question-2-answer-1-correct", true, Some(false), question2Id)), 10 seconds)
    val answer4Id = Await.result(AnswerDAO.insert(Answer(None, "test-question-2-answer-2-not-correct", false, Some(false), question2Id)), 10 seconds)

    val question1: Question = Await.result(QuestionDAO.findById(question1Id), 10 seconds)
    val question2: Question = Await.result(QuestionDAO.findById(question2Id), 10 seconds)
    val answer1: Answer = Await.result(AnswerDAO.findById(answer1Id), 10 seconds)
    val answer2: Answer = Await.result(AnswerDAO.findById(answer2Id), 10 seconds)
    val answer3: Answer = Await.result(AnswerDAO.findById(answer3Id), 10 seconds)
    val answer4: Answer = Await.result(AnswerDAO.findById(answer4Id), 10 seconds)

    //when
    Await.result(QuizProgressServiceImpl.updateResults(instanceId, Map(
      (answer1.id.get, true),
      (answer2.id.get, true),
      (answer3.id.get, true),
      (answer4.id.get, false)
    ), "uuid", "127.0.0.1"), 10 seconds)

    //then
    val instance: QuizInstance = Await.result(QuizInstanceDAO.findById(instanceId), 10 seconds)
    instance should have(
      'score(0)
    )
  }

  "QuizProgressService" should "not allow changes after finishing" in {
    // given
    val templateId = Await.result(QuizTemplateDAO.insert(QuizTemplate(None, "test-template")), 10 seconds)
    val quizId = Await.result(QuizDAO.insert(Quiz(None, DateTime.now(), 10000, "0.0.0.0/0", "test-quiz", templateId)), 10 seconds)
    val instanceId = Await.result(QuizInstanceDAO.insert(QuizInstance(None, "250042", "uuid", 0, false, quizId)), 10 seconds)

    val question1Id = Await.result(QuestionDAO.insert(Question(None, "test-question-1", instanceId)), 10 seconds)
    val answer1Id = Await.result(AnswerDAO.insert(Answer(None, "test-question-1-answer-1-correct", true, Some(false), question1Id)), 10 seconds)
    val answer2Id = Await.result(AnswerDAO.insert(Answer(None, "test-question-1-answer-2-not-correct", false, Some(false), question1Id)), 10 seconds)

    val question2Id = Await.result(QuestionDAO.insert(Question(None, "test-question-2", instanceId)), 10 seconds)
    val answer3Id = Await.result(AnswerDAO.insert(Answer(None, "test-question-2-answer-1-correct", true, Some(false), question2Id)), 10 seconds)
    val answer4Id = Await.result(AnswerDAO.insert(Answer(None, "test-question-2-answer-2-not-correct", false, Some(false), question2Id)), 10 seconds)

    val question1: Question = Await.result(QuestionDAO.findById(question1Id), 10 seconds)
    val question2: Question = Await.result(QuestionDAO.findById(question2Id), 10 seconds)
    val answer1: Answer = Await.result(AnswerDAO.findById(answer1Id), 10 seconds)
    val answer2: Answer = Await.result(AnswerDAO.findById(answer2Id), 10 seconds)
    val answer3: Answer = Await.result(AnswerDAO.findById(answer3Id), 10 seconds)
    val answer4: Answer = Await.result(AnswerDAO.findById(answer4Id), 10 seconds)

    //when
    Await.result(QuizProgressServiceImpl.updateResultsAndFinish(instanceId, Map(
      (answer1.id.get, true),
      (answer2.id.get, true),
      (answer3.id.get, true),
      (answer4.id.get, false)
    ), "uuid", "127.0.0.1"), 10 seconds) should be (true)

    Await.result(QuizProgressServiceImpl.updateResults(instanceId, Map(
      (answer1.id.get, false),
      (answer2.id.get, false),
      (answer3.id.get, false),
      (answer4.id.get, true)
    ), "uuid", "127.0.0.1"), 10 seconds) should be (false)

    //then
    val instance: QuizInstance = Await.result(QuizInstanceDAO.findById(instanceId), 10 seconds)
    instance should have(
      'submitted(true),
      'score(1)
    )
  }
}