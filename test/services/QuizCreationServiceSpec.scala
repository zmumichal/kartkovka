package services

import org.scalatest._

class QuizCreationServiceSpec extends FlatSpec with Matchers {
  "QuizCreationService" should "create quiz" in {}
  "QuizCreationService" should "deep clone quiz with all questions with all answers" in {}
  "QuizCreationService" should "deep clone quiz with all questions cloned and with all answers cloned" in {}
  "QuizCreationService" should "parse question format on adding" in {}
  "QuizCreationService" should "parse question format on editing" in {}
  "QuizCreationService" should "override old answers on editing question" in {}
  "QuizCreationService" should "preserve user questions and answers in solved quizes on editing" in {}
  "QuizCreationService" should "preserve user questions and answers in solved quizes on removing" in {}
}