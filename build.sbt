name := "toik_kartkovka"

version := "1.0"

lazy val `toik_kartkovka` = (project in file(".")).enablePlugins(PlayScala)

scalaVersion := "2.11.1"

libraryDependencies ++= Seq(jdbc, cache, ws,
  "com.typesafe.slick" %% "slick" % "3.0.0",
  "com.typesafe.play" %% "play-slick" % "1.0.0-RC2",
  "org.xerial" % "sqlite-jdbc" % "3.8.7",
  "joda-time" % "joda-time" % "2.7",
  "org.joda" % "joda-convert" % "1.7",
  "com.github.tototoshi" %% "slick-joda-mapper" % "2.0.0",
  "org.specs2" %% "specs2-core" % "3.6" % "test",
  "org.scalatest" % "scalatest_2.11" % "2.2.4" % "test"
)
libraryDependencies += "io.wasted" %% "wasted-util" % "0.10.0"

resolvers += "scalaz-bintray" at "http://dl.bintray.com/scalaz/releases"
resolvers += "wasted.io/repo" at "http://repo.wasted.io/mvn"
unmanagedResourceDirectories in Test <+= baseDirectory(_ / "target/web/public/test")